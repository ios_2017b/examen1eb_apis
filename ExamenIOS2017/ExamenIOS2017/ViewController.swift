//
//  ViewController.swift
//  ExamenIOS2017
//
//  Created by Graciela Moreno on 13/12/17.
//  Copyright © 2017 Graciela Moreno. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var passLabel: UITextField!
    @IBOutlet weak var usuarioLabel: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "loginSegue" {
            let destinationController = segue.destination as! PrincipalViewController
            destinationController.message = "\(usuarioLabel.text!)"
        }
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        usuarioLabel.resignFirstResponder()
        passLabel.resignFirstResponder()
    }
    @IBAction func ingresarButton(_ sender: Any) {
    
        performSegue(withIdentifier: "loginSegue", sender: self)
    }
    
    
}
