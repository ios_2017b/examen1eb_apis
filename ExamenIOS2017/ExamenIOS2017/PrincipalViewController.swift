//
//  PrincipalViewController.swift
//  ExamenIOS2017
//
//  Created by Graciela Moreno on 13/12/17.
//  Copyright © 2017 Graciela Moreno. All rights reserved.
//

import UIKit

class PrincipalViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet weak var tableComic: UITableView!
    @IBOutlet weak var nameUserLabel: UILabel!
    @IBOutlet weak var avatarImage: UIImageView!
    
    var message:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let bm = BackendManager()
        bm.getAllComics()
        
        NotificationCenter.default.addObserver(self, selector: #selector(reload), name: NSNotification.Name("reload"), object: nil)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        nameUserLabel.text = message!
        let bm = BackendManager()
        
        bm.getAvatar(user: nameUserLabel.text!, completionHandler: { (imageAvatar) in
            
            DispatchQueue.main.async {
                self.avatarImage.layer.cornerRadius = self.avatarImage.frame.size.width/2
                self.avatarImage.clipsToBounds = true
                self.avatarImage.image = imageAvatar
            }
        })
        
    }
    @objc func reload() {
        tableComic.reloadData()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return comicArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "comicCell") as! ComicTableViewCell
        
        
        cell.comic = comicArray[indexPath.row]
        cell.fillData()
        
        return cell
    }
    
    
    
}

