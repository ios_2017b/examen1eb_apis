//
//  ObjectMapper.swift
//  ExamenIOS2017
//
//  Created by Graciela Moreno on 13/12/17.
//  Copyright © 2017 Graciela Moreno. All rights reserved.
//

import Foundation
import ObjectMapper

class ComicApiResponse:Mappable {
    var resultados:[Comic]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        resultados <- map["data.results"]
    }
}

class Comic:Mappable {
    var id:Int?
    var title:String?
    var series:String?
    var description:String?
    var thumbnail:UIImage?
    var pathImage:String?
    
    required init(map:Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        title <- map["title"]
        description <- map["description"]
        series <- map["series.name"]
        pathImage <- map["thumbnail.path"]
    }
}

